<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class TrackingCustomerFood extends Model
{
    protected $table = 'tracking_customer_foods';
    protected $connection = 'transaction-data';

    protected $casts = [
        'servings' => 'array',
        'standard_serving' => 'array'
    ];

    protected $attributes = [
        'customer_id' => 0,
        'food_id' => 0,
        'created_at' => "",
        'food_title' => "",
        'food_brand' => "",
        'food_kcal' => 0,
        'food_kcal_producer' => 0,
        'food_protein' => 0,
        'food_fat' => 0,
        'food_carbs' => 0,
        'food_verified' => 0,
        'selected_serving_metric' => "g",
        'selected_serving_amount' => 0,
        'selected_servings' => 0,
        'selected_meal' => "",
        'standard_serving' => "",
        'servings' => ''

    ];
    protected $fillable = array('customer_id','food_id','created_at','food_title','food_brand','food_kcal','food_protein','food_fat','food_carbs','food_verified','selected_serving_metric','selected_serving_amount','selected_servings','selected_meal','standard_serving','servings');

}
