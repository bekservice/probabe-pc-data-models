<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class DietPlanSimplification extends Model
{
    protected $connection = 'transaction-data';
    protected $attributes = [
        'customer_id' => -1,
        'diet_plan_id' => -1,
        'hippo' => -1,
        'lion' => -1,
        'giraffe' => -1,
        'lama' => -1,
    ];


    protected $fillable = array('customer_id','diet_plan_id','hippo','lion','giraffe','lama');

}
