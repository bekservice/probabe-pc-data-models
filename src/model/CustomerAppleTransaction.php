<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerAppleTransaction extends Model
{
    protected $connection = 'transaction-data';
    public $timestamps = false;
    protected $attributes = [
        'customer_id' => 0,
        'customer_identifier_for_vendor' => '',
        'customer_identification_token' => '',
        'transaction_identifier' => '',
        'transaction_state' => '',
        'transaction_date' => '',
    ];
    protected $fillable = array('customer_id','customer_identifier_for_vendor','customer_identification_token','transaction_identifier', 'transaction_state','transaction_date');

}
