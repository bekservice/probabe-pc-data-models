<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;


class Document extends Model
{
    protected $connection = 'static-data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $attributes = [
        'title' => "",
        'image' => "",
        'new' => false,
        'active' => false
    ];
    protected $fillable = array('title','image', 'new', 'active');



    /**
     * Get the user that owns the phone.
     */
    public function documentcategory()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\DocumentCategory::class);
    }


    // each bear BELONGS to many picnic
    // define our pivot table also
    public function contents() {
        return $this->hasMany(\SaltCon\ProCentral\Models\DocumentContent::class);
    }
}
