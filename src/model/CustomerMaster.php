<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerMaster extends Model
{
    protected $connection = 'transaction-data';

    protected $attributes = [
        'customer_id' => 0,
        'ip' => "",
        'street' => "",
        'city' => "",
        'country_code' => "",
        'iban' => "",
    ];

    protected $fillable = array(
        'customer_id',
        'ip',
        'street',
        'city',
        'country_code',
        'iban'
    );
}
