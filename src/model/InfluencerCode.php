<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class InfluencerCode extends Model
{
    protected $connection = 'static-data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $attributes = [
        'code' => null,
        'customer_id' => null,
        'percentage' => 0
    ];
    protected $fillable = array( 'code', 'customer_id','percentage');

}
