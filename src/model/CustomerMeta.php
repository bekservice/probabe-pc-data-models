<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerMeta extends Model
{
    protected $connection = 'transaction-data';
    protected $attributes = [
        'customer_id' => 0,
        'meta_name' => '',
        'meta_value' => ''
    ];
    protected $fillable = array('customer_id','meta_name','meta_value');


}
