<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class BodyUpdate extends Model
{
    protected $connection = 'transaction-data';

    protected $attributes = [
        'weight' => "",
        'height' => "",
        'kfa' => "",
        'age' => "",
        'pal' => "",
        'diet_break' => '',
        'question_kcal_respected' => '',
        'question_constant_increase' => '',
        'question_optical_progress' => '',
        'question_clothes_looser' => '',
        'body_waist' => null,
        'body_chest' => null,
        'body_hips' => null,
        'body_legs' => null,
        'body_biceps' => null,
        'image_front' => null,
        'image_side' => null,
        'image_back' => null,
        'data' => null,
    ];


    protected $fillable = array('weight','height','kfa','age','pal','diet_break','question_kcal_respected'
    ,'question_constant_increase'
    ,'question_optical_progress'
    ,'question_clothes_looser'
    ,'body_waist'
    ,'body_chest'
    ,'body_hips'
    ,'body_legs'
    ,'body_biceps'
    ,'image_front'
    ,'image_side'
    ,'image_back'
    ,'data');



    /**
     * Get the user that owns the phone.
     */
    public function customer()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Customer::class);
    }
}
