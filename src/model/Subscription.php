<?php

namespace SaltCon\ProCentral\Models;

use Carbon\Carbon;
use Laravel\Cashier\Subscription as Model;

class Subscription extends Model
{
    protected $connection = 'subscription-data';

    protected $attributes = [
        'referrer_id' => null,
        'quantity' => 1,
        'trial_ends_at' => null,
        'ends_at' => null,
        'braintree_subscription_id' => null,
        'braintree_payment_method_token' => null,
        'braintree_status' => null,
        'novalnet_subscription_id' => null,
        'novalnet_payment_method_token' => null,
        'novalnet_status' => null,

    ];
    protected $fillable = array('referrer_id');

    public function owner()
    {
        $model = "SaltCon\ProCentral\Models\Customer";

        return $this->belongsTo($model, (new $model)->getForeignKey());
    }

    /**
     * Cancel the subscription at the end of the billing period.
     *
     * @return $this
     */
    public function cancel()
    {

        if($this->braintree_subscription_id != null){

            $url = "https://kasse.probabe.de/api/braintree/subscription/{$this->braintree_subscription_id}/cancel";

            $headers = array(
                'Accept: application/json'
            );

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');

            $response = curl_exec($curl);
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

            return $this;

        }else{

            $subscription = $this->asStripeSubscription();

            $subscription->cancel_at_period_end = true;

            $subscription = $subscription->save();

            $this->stripe_status = $subscription->status;

            // If the user was on trial, we will set the grace period to end when the trial
            // would have ended. Otherwise, we'll retrieve the end of the billing period
            // period and make that the end of the grace period for this current user.
            if ($this->onTrial()) {
                $this->ends_at = $this->trial_ends_at;
            } else {
                $this->ends_at = Carbon::createFromTimestamp(
                    $subscription->current_period_end
                );
            }

            $this->save();

            return $this;
        }
    }

}

?>