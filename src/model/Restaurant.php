<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $connection = 'static-data';


    protected $attributes = [
        'title' => "",
        'new' => false,
        'active' => false,
        'sort' => 0


    ];
    protected $fillable = array('title', 'active', 'new','sort');
}
