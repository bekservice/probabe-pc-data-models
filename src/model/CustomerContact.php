<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerContact extends Model
{
    protected $connection = 'transaction-data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $attributes = [
        'customer_id' => null,
        'firstname' => "",
        'lastname' => '',
        'email' => '',
        'street' => '',
        'house_number' => '',
        'zip' => '',
        'city' => '',
        'country' => '',

    ];
    protected $fillable = array( 'customer_id', 'firstname', 'lastname','email','street','house_number','zip', 'city', 'country');
}
