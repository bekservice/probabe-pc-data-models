<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;


class TrainingPlan extends Model
{
    protected $connection = 'static-data';
    protected $attributes = [
        'title' => "",
        'training_plan_category_id' => 0,
        'influencer' => '',
        'information' => '',
        'cardio' => '',
        'plan' => '',
        'image' => '',
        'duration' => '',
        'new' => false,
        'active' => false,
    ];
    protected $fillable = array('title','influencer','information','cardio','plan','image','duration','new','active');

    /**
     * Get the user that owns the phone.
     */
    public function trainingplanintensity()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\TrainingPlanIntensity::class);
    }

    public function trainingplancategory()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\TrainingPlanCategory::class);
    }

    // each bear BELONGS to many picnic
    // define our pivot table also
    public function exercises() {
        return $this->hasMany(\SaltCon\ProCentral\Models\TrainingPlanExercise::class);
    }


}
