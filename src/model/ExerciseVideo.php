<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class ExerciseVideo extends Model
{
    protected $connection = 'static-data';
    protected $attributes = [
        'video' => '',
        'image' => '',
    ];
    protected $fillable = array('video','image');

    public function exercise()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Exercise::class);
    }

}
