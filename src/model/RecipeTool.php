<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeTool extends Model
{
    protected $connection = 'static-data';


    protected $attributes = [
        'title' => "",


    ];
    protected $fillable = array('title');

    public function getData(){
        return $this;
    }

    public function recipe()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Recipe::class);
    }
}
