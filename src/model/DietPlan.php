<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class DietPlan extends Model
{
    protected $connection = 'transaction-data';

    protected $attributes = [
        'customer_id' => 0,
        'status' => "",
        'kfa' => "",
        'type' => "",
        'mode' => "",
        'protein_variant' => "",
        'kcal' => 0,
        'kcal_min' => 0,
        'kcal_max' => 0,
        'protein' => 0,
        'protein_min' => 0,
        'protein_max' => 0,
        'fat'  => 0,
        'fat_min' => 0,
        'fat_max' => 0,
        'carbs' => 0,
        'carbs_min' => 0,
        'carbs_max' => 0,
        'refeed_days' => 0,
        'refeed_day_data' => "",
        'physical_values' => "",
        'premium' => 0
    ];
    protected $fillable = array('customer_id',
        'status',
        'kfa',
        'type',
        'mode',
        'protein_variant',
        'kcal',
        'kcal_min',
        'kcal_max',
        'protein',
        'protein_min',
        'protein_max',
        'fat',
        'fat_min',
        'fat_max',
        'carbs',
        'carbs_min',
        'carbs_max',
        'refeed_days',
        'refeed_day_data',
        'physical_values',
        'premium');

    /**
     * Get the user that owns the phone.
     */
    public function customer()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Customer::class);
    }
}
