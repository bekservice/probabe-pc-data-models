<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeFood extends Model
{
    protected $table = 'recipe_foods';

    protected $connection = 'static-data';

    protected $attributes = [
        'serving_amount' => '1',
        'food' => '',

    ];
    protected $fillable = array('serving_amount','recipe_step_id');


    public function recipestep(){
        return $this->belongsTo(\SaltCon\ProCentral\Models\RecipeStep::class,'recipe_step_id');
    }

    public function getData(){
        return $this;
    }
}
