<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class Customer extends Authenticatable
{

    use Billable;

    protected $connection = 'transaction-data';
    protected $attributes = [
        'firstname' => "",
        'lastname' => "",
        'amazon_id' => null,
        'gender' => "",
        'email' => "",
        'height' => "",
        'password' => '',
        'api_token' => '',
        'push_device_token' => '',
        'referrer_id' => null,
        'os' => null
    ];


    protected $casts = [
        'birthdate' => 'date',
    ];

    protected $fillable = array('amazon_id','firstname','lastname','gender','email','birthdate','height','password','referrer_id','os');

    public function generateToken()
    {
        $this->api_token = str_random(60);
        $this->save();

        return $this->api_token;
    }

    public function influencer()
    {
        return $this->hasOne(\SaltCon\ProCentral\Models\Customer::class);
    }

    public function referrer()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Customer::class);
    }

    public function customerprogram()
    {
        return $this->hasOne(\SaltCon\ProCentral\Models\CustomerProgram::class);
    }

    public function customerevent()
    {
        return $this->hasOne(\SaltCon\ProCentral\Models\CustomerEvent::class);
    }

    public function customerapp()
    {
        return $this->hasOne(\SaltCon\ProCentral\Models\CustomerApp::class);
    }

    public function weight(){
        return $this->hasMany(\SaltCon\ProCentral\Models\Weight::class);
    }

    public function bodyupdate(){
        return $this->hasMany(\SaltCon\ProCentral\Models\BodyUpdate::class);
    }

    public function dietplan(){
        return $this->hasMany(\SaltCon\ProCentral\Models\DietPlan::class);
    }

    public function customermeal(){
        return $this->hasMany(\SaltCon\ProCentral\Models\CustomerMeal::class);
    }

    public function customerapplereceipt(){
        return $this->hasMany(\SaltCon\ProCentral\Models\CustomerAppleReceipt::class);
    }

    public function chatmessageincoming(){
        return $this->hasMany(\SaltCon\ProCentral\Models\ChatMessage::class, 'sender_id');
    }

    public function chatmessagesent(){
        return $this->hasMany(\SaltCon\ProCentral\Models\ChatMessage::class, 'recipient_id');
    }

    public function subscriptions()
    {
        return $this->hasMany(\SaltCon\ProCentral\Models\Subscription::class)->orderBy('created_at', 'desc');
    }
}
