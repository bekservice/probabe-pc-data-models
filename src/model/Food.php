<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Fico7489\Laravel\Pivot\Traits\PivotEventTrait;
use SaltCon\ProCentral\Pivot\FoodFoodGroup;

class Food extends Model
{
    use PivotEventTrait;
    use Searchable;
    public $asYouType = true;


    protected $table = 'foods';

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }


    public function touch()
    {
        if (! $this->usesTimestamps()) {
            return false;
        }

        $this->updateTimestamps();

        return $this->save();
    }

    protected $connection = 'static-data';
    protected $attributes = [
        'title' => "",
        'status' => 1,
        'verified' => 0,
        'ean' => "",
        'brand' => "",
        'producer' => "",
        'kcal' => '',
        'kcal_producer' => '',
        'fat' => '',
        'carbs' => '',
        'protein' => '',
        'standard_serving_metric' => '',
        'standard_serving_amount' => '',
        'standard_serving' => '',
        'saturated_fat' => '',
        'roughage' => '',
        'sugar' => ''
        /*'roughage' => '',
        'gluten' => '',
        'lactose' => '',
        'vegeterian' => '',
        'vegan' => '',
        'calcium' => '',
        'cholesterol' => '',
        'fiber' => '',
        'iron' => '',
        'mono_unsaturated_fat' => '',
        'poly_unsaturated_fat' => '',
        'saturated_fat' => '',
        'potassium' => '',
        'sodium' => '',
        'sugar' => '',
        'vitamin_a' => '',
        'vitamin_c' => ''*/
    ];


    protected $fillable = array(
        'title',
        'origin',
        'status',
        'verified',
        'ean',
        'brand',
        'producer',
        'kcal',
        'kcal_producer',
        'fat',
        'carbs',
        'protein',
        'standard_serving_metric',
        'standard_serving_amount',
        'standard_serving',
        'saturated_fat',
        'roughage',
        'sugar'
        /*'roughage',
        'gluten',
        'lactose',
        'vegeterian',
        'vegan',
        'calcium',
        'cholesterol',
        'fiber',
        'iron',
        'mono_unsaturated_fat',
        'poly_unsaturated_fat',
        'saturated_fat',
        'potassium',
        'sodium',
        'sugar',
        'vitamin_a',
        'vitamin_c'*/
    );


    public function groups()
    {
        return $this->belongsToMany(FoodGroup::class,'food_food_groups')->using(FoodFoodGroup::class);
    }

    public function servings()
    {
        return $this->belongsToMany(FoodServing::class,'food_food_servings');
    }
}
