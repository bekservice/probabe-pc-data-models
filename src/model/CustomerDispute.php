<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerDispute extends Model
{
    protected $connection = 'transaction-data';

    protected $attributes = [
        'customer_id' => 0,
        'subscription_id' => 0,
        'status' => 0,
        'stripe_plan_price' => 0,
        'invoice_id' => 0,
        'invoice_description' => null,
        'invoice_date' => null,
        'payment_reminder' => null,
        'first_warning' => null,
        'second_warning' => null,
        'third_warning' => null
    ];

    protected $casts = [
        'payment_reminder' => "date",
        'first_warning' => "date",
        'second_warning' => "date",
        'third_warning' => "date"
    ];

    protected $fillable = array('customer_id','subscription_id','stripe_plan_price','invoice_date','invoice_description','invoice_id','status','payment_reminder','first_warning','second_warning','third_warning');

    /**
     * Get the user that owns the phone.
     */
    public function subscription()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Subscription::class);
    }



    /**
     * Get the user that owns the phone.
     */
    public function customer()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Customer::class);
    }
}
