<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $connection = 'static-data';

    protected $attributes = [
        'title' => "",
        'image' => "",
        'influencer_id' => -1,
        'description' => '',
        'status' => '',
        'appearance' => '',
        'servings' => '',
        'cooking_time' => '',
        'active' => false,
    ];
    protected $fillable = array('title', 'image', 'influencer_id', 'description', 'status', 'appearance', 'servings', 'cooking_time','active');

    /**
     * Get the user that owns the phone.
     */
    public function influencer()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Customer::class);
    }

    public function recipetools(){
        return $this->hasMany(\SaltCon\ProCentral\Models\RecipeTool::class);
    }

    public function recipesteps(){
        return $this->hasMany(\SaltCon\ProCentral\Models\RecipeStep::class);
    }


}
