<?php

namespace SaltCon\ProCentral\Models;


use Illuminate\Database\Eloquent\Model;

class TrainingVideoCategory extends Model
{
    protected $connection = 'static-data';
    protected $attributes = [
        'title' => '',
    ];
    protected $fillable = array('title');

    public function videos()
    {
        return $this->hasMany(\SaltCon\ProCentral\Models\TrainingVideo::class);
    }
}
