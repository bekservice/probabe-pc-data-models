<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerDebt extends Model
{
    protected $connection = 'transaction-data';
    protected $attributes = [
        'customerId' => 0,
        'subscriptionId' => 0,
        'salutation' => 'Frau',
        'email' => '',
        'firstName' => '',
        'lastName' => '',
        'street' => '',
        'zip' => '',
        'city' => '',
        'country' => '',
        'invoiceDebt' => 0.0,
        'debtFee' => 0.0,
        'paymentProviderSubscriptionId' => '',
        'invoiceNumber' => '',
        'invoiceDate' => NULL,
        'disputeDate' => NULL,
        'totalStage' => NULL,
        'paid' => false,
        'letterFirstWarning' => NULL,
        'letterSecondWarning' => NULL,
        'emailFirstWarning' => NULL,
        'emailSecondWarning' => NULL,
        'debtCollectionStart' => NULL,
        'debtCollectionId' => ''
    ];


    protected $fillable = array('customerId',
        'subscriptionId',
        'salutation',
        'email',
        'firstName',
        'lastName',
        'street',
        'zip',
        'city',
        'country',
        'invoiceDebt',
        'debtFee',
        'paymentProviderSubscriptionId',
        'invoiceNumber',
        'invoiceDate',
        'disputeDate',
        'totalStage',
        'paid',
        'letterFirstWarning',
        'letterSecondWarning',
        'emailFirstWarning',
        'emailSecondWarning',
        'debtCollectionStart',
        'debtCollectionId');


    protected $casts = [
        'invoiceDate' => 'date',
        'disputeDate' => 'date',
        'letterFirstWarning' => 'date',
        'letterSecondWarning' => 'date',
        'emailFirstWarning' => 'date',
        'emailSecondWarning' => 'date',
        'debtCollectionStart' => 'date',
    ];

    /**
     * Get the user that owns the phone.
     */
    public function customerId()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Customer::class, 'customerId');
    }

}


