<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    protected $connection = 'static-data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $attributes = [
        'title' => "",
        'description' => "",
        'preview_image' => '',
        'image' => '',
        'video' => '',
    ];
    protected $fillable = array( 'title', 'description', 'preview_image','image','video');

    /**
     * Get the user that owns the phone.
     */
    public function exercisecategory()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\ExerciseCategory::class);
    }

    // each bear BELONGS to many picnic
    // define our pivot table also
    public function steps() {
        return $this->hasMany(\SaltCon\ProCentral\Models\ExerciseStep::class);
    }

    public function trainingplanexercise(){
        return $this->hasMany(\SaltCon\ProCentral\Models\TrainingPlanExercise::class);
    }


}
