<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class OutdoorMeal extends Model
{
    protected $connection = 'transaction-data';

    protected $attributes = [
        'title' => "",
        'image' => "",
        'customer_id' => -1,
        'description' => '',
        'message' => '',
        'status' => 0,
    ];
    protected $fillable = array('title', 'image', 'customer_id', 'description','status','message');

    /**
     * Get the user that owns the phone.
     */
    public function food()
    {
        return $this->hasMany(\SaltCon\ProCentral\Models\OutdoorMealFood::class);
    }

    public function customer()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Customer::class);
    }
}
