<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class PathElementContent extends Model
{
    protected $connection = 'static-data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $attributes = [

        'information' => "",
        'image' => "",

    ];
    protected $fillable = array('information', 'image');

    public function variant_contents() {
        return $this->hasMany(\SaltCon\ProCentral\Models\PathElementContentVariant::class,"path_element_content_id");
    }
}
