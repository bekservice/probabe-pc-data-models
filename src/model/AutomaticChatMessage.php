<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class AutomaticChatMessage extends Model
{
    //
    protected $connection = 'static-data';
    protected $attributes = [
        'key' => "",
        'target_subscription' => "",
        'days_after_registration' => "",
        'message' => ""
    ];

    protected $fillable = array('key','target_subscription','days_after_registration','message');

}
