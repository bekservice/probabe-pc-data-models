<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class FoodServing extends Model
{
    protected $connection = 'static-data';
    protected $attributes = [
        'serving_description' => "",
        'serving_amount' => "",
        'serving_unit' => "",
    ];

    protected $fillable = array('serving_description','serving_amount','serving_unit');

    public function foods()
    {
        return $this->belongsToMany(FoodServing::class,'food_food_servings');
    }
}
