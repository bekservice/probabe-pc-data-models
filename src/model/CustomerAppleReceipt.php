<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CustomerAppleReceipt extends Model
{
    protected $connection = 'transaction-data';
    protected $attributes = [
        'customer_id' => '',
        'transaction_id' => '',
        'purchase_date' => '',
        'receipt_creation_date' => '',
        'application_version' => '',
        'original_purchase_date' => '',
        'is_trial_period' => 'false',
        'receipt_encoded' => '',

        'quantity' => '',
        'version_external_identifier' => '',
        'original_transaction_id' => '',
        'cancellation_date' => '',
        'cancellation_reason' => '',
        'subscription_expiration_date' => '',
        'subscription_expiration_intent' => '',
        'subscription_retry_flag' => '',
        'subscription_auto_renew_status' => '',
        'subscription_auto_renew_preference' => '',
        'subscription_price_consent_status' => '',
    ];
    protected $fillable = array('customer_id','transaction_id','purchase_date','receipt_creation_date', 'application_version','original_purchase_date','is_trial_period','receipt_encoded',
        'quantity',
        'version_external_identifier',
        'original_transaction_id',
        'cancellation_date',
        'cancellation_reason',
        'subscription_expiration_date',
        'subscription_expiration_intent',
        'subscription_retry_flag',
        'subscription_auto_renew_status',
        'subscription_auto_renew_preference',
        'subscription_price_consent_status',
        );

}
