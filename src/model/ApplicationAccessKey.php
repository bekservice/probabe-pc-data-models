<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicationAccessKey extends Model
{
    protected $connection = 'transaction-data';
    protected $attributes = [
        'application' => "",
        'application_access_key' => "",
        'active' => false

    ];

    protected $fillable = array('application','application_access_key','active');


    public function generateAccessKey()
    {
        $this->application_access_key = str_random(60);
        $this->save();

        return $this->application_access_key;
    }
}
