<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingVideo extends Model
{
    protected $connection = 'static-data';
    protected $attributes = [
        'video_id' => "",
        'title' => '',
        'image' => '',
        'video' => '',
        'text' => '',
        'new' => false,
        'active' => false,
    ];
    protected $fillable = array('title','video_id','image','video','text','new','active');

    public function category()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\TrainingVideoCategory::class);
    }

}
