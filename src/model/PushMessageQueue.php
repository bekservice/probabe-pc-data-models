<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class PushMessageQueue extends Model
{
    public $timestamps = false;

    protected $connection = 'transaction-data';
    protected $attributes = [
        'customer_id' => "",
        'push_device_token' => "",
        'channel' => "",
        'title' => "",
        'message' => "",
        'time_to_send' => "",
        'sent' => 0
    ];

    protected $fillable = array('customer_id','push_device_token','channel','title','message','time_to_send','sent');
}
