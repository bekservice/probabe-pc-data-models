<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    protected $connection = 'static-data';
    protected $attributes = [
        'title' => "",
        'title_cleaned' => ''
    ];
    protected $fillable = array('title','title_cleaned');
}
