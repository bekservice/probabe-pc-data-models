<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class Weight extends Model
{
    protected $connection = 'transaction-data';
    protected $attributes = [
        'weight' => "",
    ];
    protected $fillable = array('weight','created_at');

    /**
     * Get the user that owns the phone.
     */
    public function customer()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Customer::class);
    }

}
