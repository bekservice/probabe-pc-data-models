<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class ContentHighlight extends Model
{
    protected $connection = 'static-data';

    protected $casts = [
        'show_from' => 'date',
        'release_date' => 'date'
    ];

    protected $attributes = [
        'image' => '',
        'title' => '',
        'type' => '',
    ];


    protected $fillable = array('title', 'image', 'type', 'show_from', 'release_date');

}
