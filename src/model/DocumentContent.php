<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentContent extends Model
{
    protected $connection = 'static-data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $attributes = [
        'text' => "",
        'image' => "",
        'video' => "",
    ];
    protected $fillable = array('text','image','video');


}
