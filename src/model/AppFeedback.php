<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class AppFeedback extends Model
{
    protected $connection = 'transaction-data';

    protected $attributes = [
        'user_id' => 0,
        'rating' => 0,
        'feedback' => "",
        'app_version' => "",
        'os' => "",

    ];

    protected $fillable = array(
        'user_id',
        'rating',
        'feedback',
        'app_version',
        'os'
    );

    public function customer()
    {
        return $this->belongsTo('SaltCon\ProCentral\Models\Customer','user_id');
    }
}