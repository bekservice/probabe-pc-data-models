<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class PathElement extends Model
{
    protected $connection = 'static-data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $attributes = [
        'title' => "",
        'optional' => "",
        'day' => "",
        'phase' => '',
        'video' => '',
        'image' => '',
        'todo_1' => '',
        'todo_2' => '',
        'todo_3' => '',
    ];
    protected $fillable = array('title', 'optional', 'day', 'phase', 'video', 'image', 'todo_1', 'todo_2', 'todo_3');

    public function contents() {
        return $this->hasMany(\SaltCon\ProCentral\Models\PathElementContent::class,"pathelement_id");
    }


}
