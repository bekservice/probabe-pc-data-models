<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class PushMessage extends Model
{
    public $timestamps = false;

    protected $connection = 'transaction-data';
    protected $attributes = [
        'channel' => "",
        'title' => "",
        'message' => ""
    ];

    protected $fillable = array('channel','title','message');
}
