<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class TrackingCustomerMealsContent extends Model
{
    protected $connection = 'transaction-data';

    protected $attributes = [
        'tracking_customer_meals_id' => 0,
        'meals_content_id' => 0,
    ];
    protected $fillable = array('tracking_customer_meals_id','meals_content_id');
}
