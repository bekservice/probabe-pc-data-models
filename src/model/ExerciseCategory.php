<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class ExerciseCategory extends Model
{
    protected $connection = 'static-data';


    protected $attributes = [
        'title' => "",
    ];
    protected $fillable = array('title');
}
