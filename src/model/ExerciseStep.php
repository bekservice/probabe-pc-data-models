<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class ExerciseStep extends Model
{
    protected $connection = 'static-data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $attributes = [
        'description' => "",

    ];
    protected $fillable = array('description');

}
