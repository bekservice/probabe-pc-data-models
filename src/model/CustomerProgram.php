<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerProgram extends Model
{
    protected $connection = 'transaction-data';

    protected $attributes = [
        'customer_id' => "",
        'role' => "",
        'height' => "",
        'is_influencer' => "",
        'next_type' => "",
        'next_mode' => '',
        'next_refeed_days' => '',
        'next_protein_variant' => '',
        'diet_break_count' => '',
        'vacation' => '',
        'wake_up' => '',
    ];

    protected $fillable = array('customer_id',
        'role',
        'height',
        'is_influencer',
        'next_type',
        'next_mode',
        'next_refeed_days',
        'next_protein_variant',
        'diet_break_count',
        'vacation',
        'wake_up');
}
