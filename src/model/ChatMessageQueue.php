<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class ChatMessageQueue extends Model
{
    //
    protected $connection = 'transaction-data';
    protected $attributes = [
        'sender_id' => -1,
        'recipient_id' => -1,
        'message' => "",
        'key' => "",
        'sent' => 0,
    ];

    protected $fillable = array('sender_id','recipient_id','message','key','sent');

}
