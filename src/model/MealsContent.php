<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class MealsContent extends Model
{
    protected $connection = 'transaction-data';

    protected $attributes = [
        'title' => ""
    ];
    protected $fillable = array('title');

}
