<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerApp extends Model
{
    protected $connection = 'transaction-data';

    protected $attributes = [
        'data_reload_needed' => 0,
        'data_reload_min_ios_version' => "",
        'data_reload_min_android_version' => "",
        'android_version' => "",
        'ios_version' => "",
        'version_data_reload' => 0,
    ];

    protected $fillable = array(
        'data_reload_needed',
        'data_reload_min_ios_version',
        'data_reload_min_android_version',
        'android_version',
        'ios_version',
        'version_data_reload',
        );
}
