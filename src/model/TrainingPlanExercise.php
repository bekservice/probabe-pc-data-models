<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class TrainingPlanExercise extends Model
{
    protected $connection = 'static-data';
    protected $attributes = [
        'tag' => "",
        'sets' => '',
        'repeats_min' => '',
        'repeats_max' => '',
    ];
    protected $fillable = array('tag','sets','repeats_min','repeats_max');


    /**
     * Get the user that owns the phone.
     */
    public function trainingplanexercisemuscle()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\TrainingPlanExerciseMuscle::class);
    }

    public function exercise()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Exercise::class);
    }
}
