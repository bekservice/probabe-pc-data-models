<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerTracking extends Model
{
    protected $connection = 'transaction-data';

    protected $attributes = [
        'customer_id' => 0,
        'type' => "",
        'report' => "",
        'data' => "",
    ];


    protected $fillable = array('customer_id','type','report','data');

}
