<?php
namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;


class VoucherTracking extends Model
{
    protected $connection = 'transaction-data';
    protected $attributes = [
        'ip' => "",
        'coupon' => ""
    ];

    protected $fillable = array('ip','coupon');
}
