<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class TrackingCustomerMeals extends Model
{
    protected $connection = 'transaction-data';

    protected $attributes = [
        'customer_id' => "",
        'selected_meal' => '',
        'type' => '',
        'size' => '',
    ];
    protected $fillable = array('customer_id','selected_meal','type','size');
}
