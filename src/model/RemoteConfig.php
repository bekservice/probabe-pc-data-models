<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class RemoteConfig extends Model
{
    protected $connection = 'static-data';
    protected $attributes = [
        'key' => "",
        'value' => ""
    ];
    protected $fillable = array('key','value');
}
