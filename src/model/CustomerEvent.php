<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerEvent extends Model
{
    protected $connection = 'transaction-data';

    protected $attributes = [


    ];


    protected $casts = [
        'next_physical_values_renew' => "date",
        'first_login' => "date",
        'last_login' => "date",
        'last_meal_update' => "date"
    ];

    protected $fillable = array('next_physical_values_renew','first_login','last_login','last_meal_update');

}
