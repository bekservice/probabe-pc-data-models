<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class PathElementContentVariant extends Model
{
    protected $connection = 'static-data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $attributes = [

        'information' => "",
        'image' => "",

    ];
    protected $fillable = array('information', 'image');

    /**
     * Get the user that owns the phone.
     */
    public function variant()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Variant::class);
    }

}
