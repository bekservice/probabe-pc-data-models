<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class FoodGroup extends Model
{
    protected $connection = 'static-data';
    protected $attributes = [
        'title' => "",
        'status' => ""
    ];

    protected $fillable = array('title','status');

    public function foods()
    {
        return $this->belongsToMany(Food::class,'food_food_groups');
    }
}
