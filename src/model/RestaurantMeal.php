<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantMeal extends Model
{
    protected $connection = 'static-data';


    protected $attributes = [
        'title' => "",


    ];
    protected $fillable = array('title');

    public function restaurant(){
        return $this->belongsTo(\SaltCon\ProCentral\Models\Restaurant::class);
    }
}
