<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeStep extends Model
{

    protected $connection = 'static-data';

    protected $attributes = [
        'description' => "",
        'non_food' => "",

    ];
    protected $fillable = array('description','non_food');

    public function recipefood(){
        return $this->hasMany(\SaltCon\ProCentral\Models\RecipeFood::class);
    }

    public function getData(){
        return $this;
    }

    public function recipe()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Recipe::class);
    }

}
