<?php

namespace SaltCon\ProCentral\Models;

use App\Casts\Serialized;
use Gregoriohc\Castable\CustomCastableModel;
use Illuminate\Database\Eloquent\Model;

class CustomerMeal extends CustomCastableModel
{
    protected $connection = 'transaction-data';

    protected $casts = [
        'meal_food' => 'serializedToJSON',
    ];

    protected $attributes = [
        'meal_title' => "",
        'meal_food' => '',
        'status' => 1,
        'customer_id' => 0,
        'version' => 1
    ];
    protected $fillable = array('meal_title','meal_food','status','version','customer_id');

    /**
     * Get the user that owns the phone.
     */
    public function customer()
    {
        return $this->belongsTo(\SaltCon\ProCentral\Models\Customer::class);
    }

    public function foods(){
        return $this->hasMany(\SaltCon\ProCentral\Models\CustomerMealFood::class);
    }

}
