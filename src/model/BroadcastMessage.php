<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class BroadcastMessage extends Model
{

    /**
     * Convert date fields to Carbon
     *
     * @var array
     */
    protected $dates = ['send_date'];


    protected $connection = 'static-data';
    protected $attributes = [
        'sent' => false,
        'message' => "",
        'user_id' => -1
    ];


    protected $fillable = array('sent','message','send_date','user_id');

    public static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->user_id = auth()->user()->id;
        });

        static::saving(function($post)
        {
            $post->message = str_replace("'","´",$post->message);
        });
    }
}
