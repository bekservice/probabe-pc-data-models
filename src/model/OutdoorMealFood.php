<?php

namespace SaltCon\ProCentral\Models;

use Illuminate\Database\Eloquent\Model;

class OutdoorMealFood extends Model
{
    protected $connection = 'transaction-data';

    protected $table = 'outdoor_meal_foods';
    protected $attributes = [
        'serving_amount' => '1',
        'food' => '',

    ];
    protected $fillable = array('serving_amount','recipe_step_id');


    public function outdoormeal(){
        return $this->belongsTo(\SaltCon\ProCentral\Models\OutdoorMeal::class,'outdoor_meal_id');
    }

    public function getData(){
        return $this;
    }
}
