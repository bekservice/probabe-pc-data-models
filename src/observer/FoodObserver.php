<?php
namespace SaltCon\ProCentral\Observer;

use SaltCon\ProCentral\Models\Food;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class FoodObserver{

    //Arman
    const API_BASE = "http://.../api/";
    const API_ENDPOINT = "foods/";

    public function created(Food $food)
    {
        $nextID = DB::connection('static-data')->table('foods')->max('id');

        $url = FoodObserver::API_BASE . FoodObserver::API_ENDPOINT . $nextID . "/sync_add";

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: 8QoJXqzn17GRVYBTfX5WGlHwHy5bquuD8NeTdmHkfSmvUzJLtdLzpsHHm93K'
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, 1);

        $response = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        Log::debug($response);

    }
    /**
     * This observer only gets triggered by Nova
     *
     * @param \App\Pivots\PlayerRole $playerRole
     */
    public function saved(Food $food)
    {
        /* VERIFIKATION */
        if($food->verified == true) {
            $url = FoodFoodGroupObserver::API_BASE . FoodFoodGroupObserver::API_ENDPOINT . $food->id . "/verify";
        }else{
            $url = FoodFoodGroupObserver::API_BASE . FoodFoodGroupObserver::API_ENDPOINT . $food->id . "/unverify";
        }

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: 8QoJXqzn17GRVYBTfX5WGlHwHy5bquuD8NeTdmHkfSmvUzJLtdLzpsHHm93K'
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        Log::debug($url);
        Log::debug($response);

        /* Allgemeine Datenanapassung */
        $url = FoodFoodGroupObserver::API_BASE . FoodFoodGroupObserver::API_ENDPOINT . $food->id . "/saved";
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: 8QoJXqzn17GRVYBTfX5WGlHwHy5bquuD8NeTdmHkfSmvUzJLtdLzpsHHm93K'
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        Log::debug($url);
        Log::debug($response);

    }

    public function deleted(Food $food)
    {

        $url = FoodObserver::API_BASE . FoodObserver::API_ENDPOINT . $food->id;

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: 8QoJXqzn17GRVYBTfX5WGlHwHy5bquuD8NeTdmHkfSmvUzJLtdLzpsHHm93K'
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");

        $response = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        Log::debug($response);
    }


}