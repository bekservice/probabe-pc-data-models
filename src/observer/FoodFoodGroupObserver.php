<?php
namespace SaltCon\ProCentral\Observer;

use SaltCon\ProCentral\Pivot\FoodFoodGroup;
use Illuminate\Support\Facades\Log;

class FoodFoodGroupObserver{

    //Arman
    const API_BASE = "http://.../api/";
    const API_ENDPOINT = "foods/";

    /**
     * This observer only gets triggered by Nova
     *
     * @param \App\Pivots\PlayerRole $playerRole
     */
    public function saved(FoodFoodGroup $group)
    {
        $url = FoodFoodGroupObserver::API_BASE.FoodFoodGroupObserver::API_ENDPOINT.$group->food_id."/group_if_needed";

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: 8QoJXqzn17GRVYBTfX5WGlHwHy5bquuD8NeTdmHkfSmvUzJLtdLzpsHHm93K'
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        Log::debug($url);
        Log::debug($response);
    }

    /**
     * This observer only gets triggered by Nova
     *
     * @param \App\Pivots\PlayerRole $playerRole
     */
    public function deleted(FoodFoodGroup $group)
    {
        $url = FoodFoodGroupObserver::API_BASE.FoodFoodGroupObserver::API_ENDPOINT.$group->food_id."/ungroup_if_needed";

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: 8QoJXqzn17GRVYBTfX5WGlHwHy5bquuD8NeTdmHkfSmvUzJLtdLzpsHHm93K'
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        Log::debug($url);
        Log::debug($response);
    }
}