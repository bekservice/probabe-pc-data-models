<?php
namespace SaltCon\ProCentral\Observer;

use SaltCon\ProCentral\Models\CustomerProgram;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class CustomerProgramObserver{

    //Arman
    const API_BASE = "http://.../api/";
    const API_ENDPOINT = "sync/customers/";


    /**
     * This observer only gets triggered by Nova
     *
     * @param \App\Pivots\PlayerRole $playerRole
     */
    public function saved(CustomerProgram $customer_program)
    {
        $url = CustomerProgramObserver::API_BASE . CustomerProgramObserver::API_ENDPOINT . $customer_program->customer_id . "/programs";

        $data = array(
            'height' => $customer_program->height,
            'next_type' => $customer_program->next_type,
            'next_mode' => $customer_program->next_mode,
            'next_refeed_days' => $customer_program->next_refeed_days,
            'next_protein_variant' => $customer_program->next_protein_variant,
            'diet_break_count' => $customer_program->diet_break_count
        );

        $payload = json_encode($data);

        Log::debug($payload);

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: 8QoJXqzn17GRVYBTfX5WGlHwHy5bquuD8NeTdmHkfSmvUzJLtdLzpsHHm93K',
            'Content-Length: ' . strlen($payload)
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');


        $response = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
    }


}