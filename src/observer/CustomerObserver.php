<?php
namespace SaltCon\ProCentral\Observer;

use SaltCon\ProCentral\Models\Customer;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class CustomerObserver{

    const API_BASE = "http://probabe.de/wp-json/probabe/v1/";
    const API_ENDPOINT = "customers/";

    // Wenn Kunde von TAPI gelöscht wird, dann muss dieser auch aus WordPress entfernt werden
    public function deleted(Customer $customer)
    {

        $url = CustomerObserver::API_BASE . CustomerObserver::API_ENDPOINT . $customer->id . "?_method=delete&email=".$customer->email;

        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");

        $response = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        Log::debug("DELETED CUSTOMER");
        Log::debug($url);
        Log::debug($response);
    }


}