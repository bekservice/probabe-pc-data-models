<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_debts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customerId')->nullable();
            $table->integer('subscriptionId')->nullable();
            $table->string('salutation')->default("Frau");
            $table->string('email')->nullable();
            $table->string('firstName')->nullable();
            $table->string('lastName')->nullable();
            $table->string('street')->nullable();
            $table->string('zip')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->double('invoiceDebt')->nullable();
            $table->double('debtFee')->nullable();
            $table->string('paymentProviderSubscriptionId')->nullable();
            $table->string('invoiceNumber')->nullable();
            $table->timestamp('invoiceDate')->nullable();
            $table->timestamp('disputeDate')->nullable();
            $table->boolean('paid')->default(false);
            $table->timestamp('letterFirstWarning')->nullable();
            $table->timestamp('letterSecondWarning')->nullable();
            $table->timestamp('emailFirstWarning')->nullable();
            $table->timestamp('emailSecondWarning')->nullable();
            $table->timestamp('debtCollectionStart')->nullable();
            $table->string('debtCollectionId')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_debts');
    }
}
