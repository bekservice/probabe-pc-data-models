<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("static-data")->create('restaurant_meals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('restaurant_id');
            $table->string("title");
            $table->string("ingredients")->nullable();
            $table->string("notes")->nullable();

            $table->double("kcal");
            $table->double("protein");
            $table->double("fat");
            $table->double("carbs");


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("static-data")->dropIfExists('restaurant_meals');
    }
}
