<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerAppleTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("transaction-data")->create('customer_apple_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("customer_id")->nullable();
            $table->string("customer_identifier_for_vendor")->nullable();
            $table->string("customer_identification_token")->nullable();
            $table->string("transaction_identifier")->nullable();
            $table->string("transaction_state")->nullable();
            $table->string("transaction_date")->nullable();
            $table->unique([ 'transaction_identifier']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("transaction-data")->dropIfExists('customer_apple_transactions');
    }
}
