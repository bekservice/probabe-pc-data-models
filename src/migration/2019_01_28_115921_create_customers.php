<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("sqlite")->create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string("firstname")->nullable();
            $table->string("lastname")->nullable();
            $table->string("gender");
            $table->string("email");
            $table->timestamp("birthdate")->nullable();
            $table->double("height")->nullable();
            $table->integer("referrer_id")->nullable();

            $table->string('api_token', 60)->nullable();
            $table->string('push_device_token',100)->nullable();
            $table->string('password', 60)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("sqlite")->dropIfExists('customers');
    }
}
