<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerAppleReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("sqlite")->create('customer_apple_receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("customer_id");
            $table->string("product_id");
            $table->integer("quantity");
            $table->string("application_version");
            $table->string("version_external_identifier");
            $table->string("transaction_id");
            $table->string("original_transaction_id");
            $table->string("purchase_date");
            $table->string("receipt_creation_date");
            $table->string("original_purchase_date");
            $table->string("cancellation_date");
            $table->integer("cancellation_reason");
            $table->string("is_trial_period");
            $table->string("subscription_expiration_date");
            $table->integer("subscription_expiration_intent");
            $table->integer("subscription_retry_flag");
            $table->integer("subscription_auto_renew_status");
            $table->integer("subscription_auto_renew_preference");
            $table->integer("subscription_price_consent_status");

            $table->text("receipt_encoded");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("sqlite")->dropIfExists('customer_apple_receipts');
    }
}
