<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutdoorMealFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("transaction-data")->create('outdoor_meal_foods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("outdoor_meal_id");
            $table->text("food");
            $table->string("serving_type");
            $table->string("serving_amount");
            $table->timestamps();

            $table->index('outdoor_meal_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("transaction-data")->dropIfExists('outdoor_meal_foods');
    }
}
