<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("transaction-data")->create('customer_programs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("customer_id");
            $table->string("role")->default("member");
            $table->double("height")->nullable();
            $table->boolean("is_influencer")->default(false);

            $table->string("next_type")->nullable();
            $table->string("next_mode")->nullable();
            $table->integer("next_refeed_days")->default(0);
            $table->string("next_protein_variant")->nullable();

            $table->integer("diet_break_count")->default(0);
            $table->boolean("vacation")->default(false);
            $table->string("wake_up")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("transaction-data")->dropIfExists('customer_programs');
    }
}
