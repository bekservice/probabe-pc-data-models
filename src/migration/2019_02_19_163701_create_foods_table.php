<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("static-data")->create('foods', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title');
            $table->string('origin')->nullable();
            $table->boolean('status');
            $table->boolean('verified');
            $table->string('ean')->nullable();
            $table->string('brand')->nullable();
            $table->string('producer')->nullable();

            $table->double('kcal');
            $table->double('kcal_producer')->nullable();
            $table->double('fat');
            $table->double('carbs');
            $table->double('protein');

            $table->string('standard_serving_metric');
            $table->string('standard_serving_amount');
            $table->string('standard_serving');

            $table->double('roughage')->nullable();
            $table->double('gluten')->nullable();
            $table->double('lactose')->nullable();
            $table->double('vegeterian')->nullable();
            $table->double('vegan')->nullable();
            $table->double('calcium')->nullable();
            $table->double('cholesterol')->nullable();
            $table->double('fiber')->nullable();
            $table->double('iron')->nullable();
            $table->double('mono_unsaturated_fat')->nullable();
            $table->double('poly_unsaturated_fat')->nullable();
            $table->double('saturated_fat')->nullable();
            $table->double('potassium')->nullable();
            $table->double('sodium')->nullable();
            $table->double('sugar')->nullable();
            $table->double('vitamin_a')->nullable();
            $table->double('vitamin_c')->nullable();

            $table->integer('customer_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("static-data")->dropIfExists('foods');
    }
}
