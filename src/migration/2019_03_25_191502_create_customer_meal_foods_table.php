<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerMealFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("transaction-data")->create('customer_meal_foods', function (Blueprint $table) {
            $table->increments('id');

            $table->integer("customer_meal_id");
            $table->integer("food_id");
            $table->timestamps();

            $table->string("food_title");
            $table->string("food_brand")->nullable();
            $table->double("food_kcal");
            $table->double("food_protein");
            $table->double("food_fat");
            $table->double("food_carbs");
            $table->string("selected_serving_metric");
            $table->double("selected_serving_amount");
            $table->double("selected_servings");

            $table->text('standard_serving');
            $table->text('servings');



            $table->index('customer_meal_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('transaction')->dropIfExists('customer_meal_foods');
    }
}
