<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDietPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("transaction-data")->create('diet_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("customer_id");
            $table->string('status')->nullable();
            $table->string("kfa");
            $table->string("type");
            $table->string("mode");
            $table->string("protein_variant")->nullable();

            $table->timestamps();

            $table->integer("kcal");
            $table->integer("kcal_min");
            $table->integer("kcal_max");

            $table->integer("protein");
            $table->integer("protein_min");
            $table->integer("protein_max");

            $table->integer("fat");
            $table->integer("fat_min");
            $table->integer("fat_max");

            $table->integer("carbs");
            $table->integer("carbs_min");
            $table->integer("carbs_max");

            $table->integer("refeed_days")->nullable();
            $table->string("refeed_day_data")->nullable();
            $table->text("physical_values")->nullable();
            $table->integer('premium');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("transaction-data")->dropIfExists('diet_plans');
    }
}
