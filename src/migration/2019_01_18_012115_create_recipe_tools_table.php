<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeToolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("static-data")->create('recipe_tools', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("recipe_id");
            $table->string("title");

            $table->timestamps();

            $table->index('recipe_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("static-data")->dropIfExists('recipe_tools');
    }
}
