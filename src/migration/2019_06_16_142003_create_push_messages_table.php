<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("transaction-data")->create('push_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('channel');
            $table->string('title')->nullable();
            $table->text('message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("transaction-data")->dropIfExists('push_messages');
    }
}
