<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerDisputesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("transaction-data")->create('customer_disputes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('subscription_id');
            $table->integer('status')->default(0);
            $table->timestamp('payment_reminder')->nullable();
            $table->timestamp('first_warning')->nullable();
            $table->timestamp('second_warning')->nullable();
            $table->timestamp('third_warning')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("transaction-data")->dropIfExists('customer_disputes');
    }
}
