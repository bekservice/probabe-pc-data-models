<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("transaction-data")->create('customer_meals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("customer_id");
            $table->boolean("status");
            $table->string("meal_title");
            $table->text("meal_food");
            $table->int("version")->default('1');

            $table->timestamps();
            $table->unique(['status','customer_id','meal_title']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("transaction-data")->dropIfExists('customer_meals');
    }
}
