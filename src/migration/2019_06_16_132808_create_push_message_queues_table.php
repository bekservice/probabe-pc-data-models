<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushMessageQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("transaction-data")->create('push_message_queues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id');
            $table->string('push_device_token', 100);
            $table->string('channel');
            $table->string('title')->nullable();
            $table->text('message');
            $table->timestamp('time_to_send');
            $table->boolean('sent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("transaction-data")->dropIfExists('push_message_queues');
    }
}
