<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection("static-data")->create('training_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('training_plan_category_id');

            $table->integer("influencer");
            $table->text('information');
            $table->text('cardio');
            $table->text('plan');
            $table->string('image');
            $table->integer('duration');

            $table->rememberToken();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("static-data")->dropIfExists('training_plans');
    }
}
