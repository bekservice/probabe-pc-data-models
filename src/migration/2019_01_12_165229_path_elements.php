<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PathElements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("static-data")->create('path_elements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->boolean('optional');
            $table->integer('day');
            $table->string('phase');
            $table->string('video')->nullable();
            $table->string('image')->nullable();
            $table->string('todo_1')->nullable();
            $table->string('todo_2')->nullable();
            $table->string('todo_3')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("static-data")->dropIfExists('path_elements');
    }
}
