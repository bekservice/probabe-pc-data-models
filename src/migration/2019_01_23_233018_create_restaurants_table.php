<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("static-data")->create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->string("title");
            $table->boolean('new');
            $table->boolean("active");
            $table->integer("sort");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("static-data")->dropIfExists('restaurants');
    }
}
