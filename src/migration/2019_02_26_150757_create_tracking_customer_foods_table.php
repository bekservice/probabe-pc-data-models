<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingCustomerFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("transaction-data")->create('tracking_customer_foods', function (Blueprint $table) {
            $table->increments('id');

            $table->integer("customer_id");
            $table->integer("food_id");
            $table->timestamps();

            $table->string("food_title");
            $table->string("food_brand")->nullable();
            $table->double("food_kcal");
            $table->double("food_protein");
            $table->double("food_fat");
            $table->double("food_carbs");
            $table->string("selected_serving_metric");
            $table->double("selected_serving_amount");
            $table->double("selected_servings");
            $table->string("selected_meal");

            $table->text('standard_serving');
            $table->text('servings');


            // Hinweis, man muss mitteilen, dass er von updated_at nur 15 Zeichen nimmt, sonst sagt MYSQL Schlüssel zu lang
            $table->unique(['customer_id','food_id','created_at','selected_serving_amount','selected_servings','selected_meal','status','updated_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("transaction-data")->dropIfExists('tracking_customer_foods');
    }
}
