<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeFoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("static-data")->create('recipe_foods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("recipe_step_id");
            $table->text("food");
            $table->boolean("swappable");
            $table->string("serving_type");
            $table->string("serving_amount");

            $table->timestamps();

            $table->index('recipe_step_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("static-data")->dropIfExists('recipe_foods');
    }
}
