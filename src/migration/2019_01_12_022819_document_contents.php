<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DocumentContents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("static-data")->create('document_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('document_id');
            $table->text('text')->nullable();
            $table->string('image')->nullable();
            $table->string('video')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::connection("static-data")->dropIfExists('document_contents');

    }
}
