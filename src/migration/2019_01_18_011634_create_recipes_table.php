<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("static-data")->create('recipes', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active');
            $table->string("title");
            $table->string("image")->nullable();
            $table->integer("influencer_id")->nullable();
            $table->string("recipe_category");
            $table->string("recipe_sub_category");

            $table->text("description");
            $table->boolean("status");
            $table->string("appearance");
            $table->integer("servings");
            $table->integer("cooking_time");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("static-data")->dropIfExists('recipes');
    }
}
