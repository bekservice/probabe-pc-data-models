<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("transaction-data")->create('customer_apps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("customer_id");
            $table->integer("data_reload_needed")->default(0);
            $table->string("data_reload_min_ios_version")->nullable();
            $table->string("data_reload_min_android_version")->nullable();
            $table->string("android_version")->nullable();
            $table->string("ios_version")->nullable();
            $table->integer("version_data_reload")->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("transaction-data")->dropIfExists('customer_apps');
    }
}
