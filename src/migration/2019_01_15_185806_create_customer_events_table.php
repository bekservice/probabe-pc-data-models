<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("transaction-data")->create('customer_events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("customer_id");
            $table->string("next_physical_values_renew")->nullable();
            $table->string("first_login")->nullable();
            $table->string("last_login")->nullable();
            $table->string("last_meal_update")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("transaction-data")->dropIfExists('customer_events');
    }
}
