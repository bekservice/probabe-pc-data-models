<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodyUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("transaction-data")->create('body_updates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("customer_id");
            $table->double("weight");
            $table->integer("height");
            $table->integer("kfa");
            $table->integer("age");
            $table->double("pal");

            $table->boolean("diet_break");
            $table->boolean("question_kcal_respected");
            $table->boolean("question_constant_increase");
            $table->boolean("question_optical_progress");
            $table->boolean("question_clothes_looser");

            $table->double("body_waist")->nullable();
            $table->double("body_chest")->nullable();
            $table->double("body_hips")->nullable();
            $table->double("body_legs")->nullable();
            $table->double("body_biceps")->nullable();

            $table->text("image_front")->nullable();
            $table->text("image_side")->nullable();
            $table->text("image_back")->nullable();
            $table->text("data")->nullable();

            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("transaction-data")->dropIfExists('body_updates');
    }
}
