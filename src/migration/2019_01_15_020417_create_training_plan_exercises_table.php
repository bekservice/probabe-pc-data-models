<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingPlanExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("static-data")->create('training_plan_exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("trainingplanexercisemuscle_id");
            $table->integer("exercise_id");
            $table->integer("sets");
            $table->integer("repeats_min");
            $table->integer("repeats_max");

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection("static-data")->dropIfExists('training_plan_exercises');
    }
}
