<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection("static-data")->create('training_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('video_id');
            $table->boolean('new');
            $table->boolean('active');
            $table->string('title');
            $table->string('image');
            $table->string('video');
            $table->text('text');
            $table->integer('category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('static-data')->dropIfExists('training_videos');
    }
}
