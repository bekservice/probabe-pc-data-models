<?php
/**
 * Created by PhpStorm.
 * User: bjornbieniakiewicz
 * Date: 2019-02-21
 * Time: 01:08
 */

namespace SaltCon\ProCentral\Pivot;

use Illuminate\Database\Eloquent\Relations\Pivot;

class FoodFoodGroup extends Pivot
{
    protected $connection = 'static-data';
    protected $table = 'food_food_groups';
    /**
     * Delete the pivot model record from the database.
     *
     * Added eloquent.deleting and eloquent.deleted events
     * to the deleted method to combine with Nova.
     *
     * @return int
     */
    public function delete()
    {
        event('eloquent.deleting: ' . __CLASS__, $this);

        parent::delete();

        event('eloquent.deleted: ' . __CLASS__, $this);
    }
}